"""
Downloads websites in threads, writes respones to a dict and prints on change.
"""

from __future__ import print_function

import logging
import random
import time
import urllib2
from Queue import Queue
from threading import Thread

import requests

logging.basicConfig(level=logging.WARN,
                    format='(%(threadName)-9s) %(message)s',
                    )


def backlog():
    with open('urls.txt', 'r') as url_file:
        urls = [url.rstrip('\n') for url in url_file.readlines()]
    return urls


timestamps = dict()  # maps url --> (timestamp, content)


class workerThread (Thread):

    """Downloads a resource and puts responses in the global dict."""

    def __init__(self, url):
        """
        @url: the url to download
        """

        Thread.__init__(self)
        self.url = url

    def run(self):
        global timestamps
        while True:
            # download
            response = requests.get('http://' + self.url)

            # modify global state
            timestamps[self.url] = (int(time.time()), response)


def main():
    # avoid RuntimeError: dictionary changed size during iteration
    global timestamps

    for url in backlog()[:PARALELLISM]:
        queue = Queue()
        worker = workerThread(url=url)
        worker.setDaemon(True)
        worker.start()

    last_shown = 0
    while True:
        if timestamps == dict():
            time.sleep(DELAY_POLL)
            continue

        # get the newest timestamp
        newest = max(item[1][0] for item in timestamps.iteritems())

        # skip if there is nothing new
        if not newest > last_shown:
            time.sleep(DELAY_POLL)
            continue
        last_shown = newest

        # show current status
        entries = []
        for key in timestamps.iterkeys():
            entries.append(': '.join([key, str(timestamps[key][0])]))
        print(', '.join(entries))


if __name__ == '__main__':
    PARALELLISM = 100  # monitor this many domains in parallel
    DELAY_POLL = 0.1  # poll the global dict for updates with this delay in s

    main()
