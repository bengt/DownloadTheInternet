"""
This thing only updates the output, once the last thread has produced new data.
In other words, we are always dropping a lot of updates.
This is undesirable, so move on to DownloaderGlobals.py
"""
from __future__ import print_function

import logging
import random
import time
from Queue import Queue
from threading import Thread

logging.basicConfig(level=logging.INFO,
                    format='(%(threadName)-9s) %(message)s',
                    )


def backlog():
    with open('urls.txt', 'r') as url_file:
        urls = [url.rstrip('\n') for url in url_file.readlines()]
    return urls


class workerThread (Thread):
    """Downloads a resource and puts repsonses in a queue."""

    def __init__(self, url, interval, queue):
        """
        @url: the url to download
        @interval: interval at which to redownload
        @queue: where to put the responses
        """

        Thread.__init__(self)
        self.url = url
        self.interval = interval
        self.queue = queue

    def run(self):
        while True:
            logging.debug('Downloading: %s' % self.url)
            # download
            timestamp = int(time.time())
            self.queue.put(timestamp)
            logging.debug('Downloaded: %s' % self.url)

            logging.debug('Sleeping: %s' % self.interval)
            time.sleep(self.interval)
            logging.debug('Slept: %s' % self.interval)


def main():
    print('setting up')

    queues = []
    for url in backlog()[:6]:
        queue = Queue()
        interval = random.random()
        worker = workerThread(url=url, interval=interval, queue=queue)
        worker.setDaemon(True)
        worker.start()
        queues.append(queue)

    print('monitoring')

    while True:
        timestamps = []
        for queue in queues:
            timestamp = queue.get()
            timestamps.append(timestamp)
            queue.task_done()
        print(timestamps)

if __name__ == '__main__':
    main()
